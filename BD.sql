-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.39-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para universidad
CREATE DATABASE IF NOT EXISTS `universidad` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `universidad`;

-- Volcando estructura para tabla universidad.patients
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `document_type` enum('T.I.','C.C.','C.E.') NOT NULL,
  `document` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `document` (`document`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 COMMENT='Datos de los pacientes registrados en el hospital';

-- Volcando datos para la tabla universidad.patients: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` (`id`, `firstname`, `lastname`, `document_type`, `document`, `city`, `address`, `phone`, `created_at`) VALUES
	(1, 'Veronica', 'Murphy', 'C.C.', 55, 'Omnis sint eveniet', 'Rerum voluptatem co', 46, '2019-05-19 15:58:12'),
	(4, 'Hyacinth', 'Patrick', 'C.C.', 15, 'Culpa unde eos repe', 'Quam perspiciatis p', 79, '2019-05-19 16:03:28'),
	(16, 'Cooper', 'Barnett', 'T.I.', 6, 'Atque at quaerat tem', 'Quod ut ratione cupi', 4, '2019-05-19 16:29:36'),
	(18, 'Kendall', 'Herring', 'C.C.', 40, 'Fugiat eligendi num', 'Alias doloremque max', 8, '2019-05-19 16:30:13'),
	(34, 'Herman', 'Jensen', 'C.C.', 4, 'Delectus eu delenit', 'Qui sit ea nulla pla', 18, '2019-05-19 17:08:32'),
	(35, 'Xantha', 'Trujillo', 'C.E.', 62, 'Magni placeat aliqu', 'Ut pariatur Reprehe', 64, '2019-05-19 17:09:10'),
	(36, 'Gannon', 'Wall', 'C.C.', 34, 'Nemo illum repudian', 'Reprehenderit aut m', 42, '2019-05-19 18:02:19');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.treatments
CREATE TABLE IF NOT EXISTS `treatments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `discount` enum('true','false') NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.treatments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `treatments` DISABLE KEYS */;
INSERT INTO `treatments` (`id`, `name`, `amount`, `discount`, `created_at`) VALUES
	(1, 'Kerry', 89, '', '2019-05-19 20:43:49');
/*!40000 ALTER TABLE `treatments` ENABLE KEYS */;

-- Volcando estructura para tabla universidad.treatment_applied
CREATE TABLE IF NOT EXISTS `treatment_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patients_id` int(11) NOT NULL DEFAULT '0',
  `treaments_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla universidad.treatment_applied: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `treatment_applied` DISABLE KEYS */;
/*!40000 ALTER TABLE `treatment_applied` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
