<?php
namespace App;
class Conection {
    static private $instance = null;
    static private $conection;
    static private $results;
    private function __contruct() {
    }
     
    public static function getInst() {
        if (self::$instance == null) {
            self::$instance = new Conection();
        }
        return self::$instance;
    }
    public function connectToDB() {
        self::$conection=new \mysqli('localhost','root', '',  'universidad');
        return self::$conection;
    }

    public function getConection(){
        return self::$conection;
    }

    public function getResults(){
        return self::$results;
    }
     
    /**
     * selectQuery
     *
     * @param string $table
     * @param string $conditions=false
     * @param mixed $select=null
     * @return void
     */
    public function selectQuery($table, $conditions=false, $select=null) {
        $sql='SELECT * ';
        $sql.="FROM ".$table;
        if($conditions){
            $sql.=" WHERE ".$conditions;
        }

        self::$results=self::connectToDB()->query($sql);
        if(self::$results){
            return true;
        }else{
            return false;
        }        
    }
     
    /**
     * insertQuery
     *
     * @param string $table
     * @param array $data
     * @return void
     */
    public function insertQuery($table,$data) {
        $sql="INSERT INTO $table ";
        $sql .= " (".implode(", ", array_keys($data)).")";
        $sql .= " VALUES ('".implode("', '", $data)."') ";

        if(self::connectToDB()->query($sql)){
            return true;
        }else{
            return false;
        }
    }

    public function updateQuery($table,$data,$id){
        $sql="UPDATE $table ";
        $sql .= "SET ";
        $fields='';
        foreach($data as $key=>$val){
            $fields .=" $key='$val',";
        }
        $sql .=substr_replace($fields ,"",-1);
        $sql .=" WHERE id='$id'";

        if(self::connectToDB()->query($sql)){
            return true;
        }else{
            return false;
        }
    }
    
}
