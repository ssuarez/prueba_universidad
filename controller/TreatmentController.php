<?php
require_once('Models/TreatmentModel.php');
use App\Models\TreatmentModel;

class TreatmentController
{
    public function index()
    {
        //Llamada a la vista
        require_once("views/forms/treatment.phtml");
    }

    /**
     * register
     *
     * @return void
     */
    public function register()
    {
       
        $treatment = new TreatmentModel();
        //Si el registro trae el id, procedemos a actualizar la informacion
        if(isset($_REQUEST["id"]) && $_REQUEST["id"]){
            if ($treatment->update($_REQUEST["id"])) {
                header('Location: /' . PROJECT_URL);
            }
        }else{
            if ($treatment->save()) {
                header('Location: /' . PROJECT_URL);
            }
        }
 
    }

    /**
     * report
     *
     * @return void
     */
    public function report()
    {
        $treatments = new TreatmentModel();
        $data = $treatments->selectAll();
        //Llamada a la vista
        require_once("views/reports/treatment.phtml");
    }

    public function edit(){
        if(isset($_REQUEST["id"])){
            $treatments = new TreatmentModel();
            $data = $treatments->findById($_REQUEST["id"]); 
            require_once("views/forms/treatment.phtml"); 
        }else{
            header('Location: /'.PROJECT_URL);
        }
    }
}
