<?php namespace App\Controller;

class Route
{
    public $project_url;
    public $controller;
    public $method;
    public $home = 'Home';

    /**
     * __construct
     *
     * @param string $url
     * @return void
     */
    public function __construct($url)
    {
        $url = explode('/', $url);
        $this->project_url = $url[1];

        //Realizamos la asignacion respectiva a cada
        // una de las variables segun su posicion en la url
        if (isset($url[2])) {
            $file = $url[2];
        }

        if (isset($url[3])) {
            $this->controller = $url[3];
        }

        if (isset($url[4])) {
            $temp=explode('?',$url[4]);
            if(count($temp)){
                $this->method = $temp[0];
            }else{
                $this->method = $url[4];
            }

        }
    }

    /**
     * load
     * Carga el metodo solicitado por url
     * @return void
     */
    function load()
    {
        if ($this->controller) {
            require_once('controller/' . ucfirst($this->controller) . 'Controller.php'); //Ubicamos el archivo del controlador
            $objName= ucfirst($this->controller).'Controller'; //Construimos el nombre de la clase
            $obj = new $objName(); // Instanciamos la clase
            if ($this->method) {
                $obj->{$this->method}();
            } else {
                $obj->index();
            }
            return $obj;
        } else {
            require_once('controller/' . $this->home . 'Controller.php');
        }
    }
}
