<?php
require_once('Models/PatientModel.php');
use App\Models\PatientModel;
class PatientsController
{
    public function index()
    {
        //Llamada a la vista
        require_once("views/forms/patients.phtml");
    }

    public function register()
    {
        $patient= new PatientModel();
        if($patient->save()){
            header('Location: /'.PROJECT_URL);
        }
    }

    public function report(){
        $patient= new PatientModel();
        $data=$patient->selectAll();
        //Llamada a la vista
        require_once("views/reports/patients.phtml");
    }
}
