<?php
require_once('Models/TreatmentAppliedModel.php');
require_once('Models/TreatmentModel.php');
require_once('Models/PatientModel.php');

use App\Models\TreatmentAppliedModel;
use App\Models\TreatmentModel;
use App\Models\PatientModel;

class TreatmentappliedController
{
    public function index()
    {
        $treatments = new TreatmentModel();
        $treatmentsSelect = $treatments->selectAll();

        $patient= new PatientModel();
        $patientSelect=$patient->selectAll();
        //Llamada a la vista
        require_once("views/forms/treatmentapplied.phtml");
    }

    /**
     * register
     *
     * @return void
     */
    public function register()
    {
        
        $treatment = new TreatmentAppliedModel();
        if ($treatment->save()) {
            header('Location: /' . PROJECT_URL);
        }
    }

    /**
     * report
     *
     * @return void
     */
    public function report()
    {
        $treatments = new TreatmentAppliedModel();
        $data = $treatments->selectAll();
        //Llamada a la vista
        require_once("views/reports/treatment.phtml");
    }

    public function edit()
    {
        if (isset($_REQUEST["id"])) {
            $treatments = new TreatmentAppliedModel();
            $data = $treatments->findById($_REQUEST["id"]);
            require_once("views/forms/treatment.phtml");
        } else {
            header('Location: /' . PROJECT_URL);
        }
    }
}
