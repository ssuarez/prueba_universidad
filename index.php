<?php
require_once("config.php");
require_once("controller/route.php");
use App\Controller\Route;

// Cargamos la ruta solicitada al index.php
$route=new Route($_SERVER['REQUEST_URI']);
define('PROJECT_URL',$route->project_url);
$route->load();
