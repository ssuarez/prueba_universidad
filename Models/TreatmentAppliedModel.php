<?php namespace App\Models;

require_once('database/Conection.php');
use App\Conection;

class TreatmentAppliedModel
{

    public $fields;
    public $table = "treatment_applied";

    /**
     * fill
     *
     * @param mixed $data
     * @return void
     */
    public function fill($data)
    {
        $this->fields["patients_id"] = $data["patients_id"];
        $this->fields["treaments_id"] = $data["treaments_id"];
    }

    /**
     * save
     *
     * @return void
     */
    public function save()
    {

        $this->fill($_REQUEST);

        if (Conection::insertQuery($this->table, $this->fields)) {
            return true;
        } else {
            echo Conection::getConection()->error;
        }
    }

    /**
     * selectAll
     *
     * @return void
     */
    public function selectAll()
    {
        if (Conection::selectQuery($this->table)) {
            return Conection::getResults()->fetch_all(MYSQLI_ASSOC);
        } else {
            echo Conection::getConection()->error;
        }
    }

    /**
     * findById
     *
     * @param int $id
     * @return void
     */
    public function findById($id)
    {
        if (Conection::selectQuery($this->table, "id='$id'")) {
            return Conection::getResults()->fetch_assoc();
        } else {
            echo Conection::getConection()->error;
        }
    }
}
