<?php namespace App\Models;
require_once('database/Conection.php');
use App\Conection;
class PatientModel{

    public $fields;
    public $table ="patients";

    /**
     * fill
     *
     * @param mixed $data
     * @return void
     */
    public function fill($data){
        $this->fields["firstname"]=$data["firstname"];
        $this->fields["lastname"]=$data["lastname"];
        $this->fields["document_type"]=$data["document_type"];
        $this->fields["document"]=$data["document"];
        $this->fields["city"]=$data["city"];
        $this->fields["address"]=$data["address"];
        $this->fields["phone"]=$data["phone"];
    }
    
    /**
     * save
     *
     * @return void
     */
    public function save(){
        $this->fill($_REQUEST);

        if(Conection::insertQuery($this->table,$this->fields)){
            return true;
        }else{
            echo Conection::getConection()->error;
        }
    }

    public function selectAll($id=false){
        if(Conection::selectQuery($this->table)){
            return Conection::getResults()->fetch_all(MYSQLI_ASSOC);
        }else{
            echo Conection::getConection()->error;
        }
    }
}