<?php namespace App\Models;

require_once('database/Conection.php');
use App\Conection;

class TreatmentModel
{

    public $fields;
    public $table = "treatments";

    /**
     * fill
     *
     * @param mixed $data
     * @return void
     */
    public function fill($data)
    {
        $this->fields["name"] = $data["name"];
        $this->fields["amount"] = $data["amount"];
        $this->fields["discount"] = $data["discount"];
    }

    /**
     * save
     *
     * @return void
     */
    public function save()
    {
        $this->fill($_REQUEST);

        if (Conection::insertQuery($this->table, $this->fields)) {
            return true;
        } else {
            echo Conection::getConection()->error;
        }
    }

    /**
     * selectAll
     *
     * @return void
     */
    public function selectAll()
    {
        if (Conection::selectQuery($this->table)) {
            return Conection::getResults()->fetch_all(MYSQLI_ASSOC);
        } else {
            echo Conection::getConection()->error;
        }
    }

    /**
     * findById
     *
     * @param int $id
     * @return void
     */
    public function findById($id)
    {
        if (Conection::selectQuery($this->table, "id='$id'")) {
            return Conection::getResults()->fetch_assoc();
        } else {
            echo Conection::getConection()->error;
        }
    }

    public function update($id)
    {
        $this->fill($_REQUEST);
        if (Conection::updateQuery($this->table, $this->fields, $id)) {
            return true;
        } else {
            echo Conection::getConection()->error;
        }
       
    }
}
